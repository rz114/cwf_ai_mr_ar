#!/usr/bin/python                                                              

import roslib; 
import rospy
import sys
from geometry_msgs.msg import Twist

class Walk(object):

    def __init__(self):
        rospy.init_node('walk_reader_node')
        self._init_params()
        self._init_pubsub()
        self.fh = open(self.file)

    def _init_pubsub(self):
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)

    def _init_params(self):
        self.file = rospy.get_param('~file', 'f.txt')

    def run(self):
        r = rospy.Rate(10.0)
        while not rospy.is_shutdown():
            for line in self.fh:
                #l = line.split()
                cmd = Twist()
                cmd.linear.x = float(line)
                #cmd.angular.z = float(l[1])
                self.cmd_vel_pub.publish(cmd)
                r.sleep()
            self.fh.close()
            exit()

if __name__ == '__main__':
    w = Walk()
    w.run()
