# cwf_ai_mr_ar

The repo contains the joint project with a blend of Mobile Robotics and AI in robotics.

# AI --> ai cw code
It demonstrates the concept of reinforcement learning on Turtlebot.

# AR-RC --> Application of Robotics cw code
It included application customized for turtlebot for operation on phone by tilting using sesnors. Inspired from RootCA App.

# MR --> Mobile Robotics cw code

It includes two folders:
ACodes --> the actual code used for project
TestCodes --> used for testing streming and laser scan code for obstacle avoidance (Check Documentation)
